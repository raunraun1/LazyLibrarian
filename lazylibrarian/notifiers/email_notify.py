# This file is part of LazyLibrarian.
#
# LazyLibrarian is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LazyLibrarian is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LazyLibrarian.  If not, see <http://www.gnu.org/licenses/>.

import smtplib
import ssl
import cherrypy
import uuid
from email.utils import formatdate, formataddr
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage

import lazylibrarian
import os
import traceback
from lazylibrarian import logger, database, ebook_convert
from lazylibrarian.scheduling import notifyStrings, NOTIFY_SNATCH, NOTIFY_DOWNLOAD, NOTIFY_FAIL
from lazylibrarian.common import is_valid_email, path_isfile, syspath, run_script, mime_type
from lazylibrarian.formatter import check_int, get_list, make_utf8bytes, unaccented


class EmailNotifier:
    def __init__(self):
        pass

    @staticmethod
    def _notify(message, event, force=False, files=None, to_addr=None):
        # suppress notifications if the notifier is disabled but the notify options are checked
        if not lazylibrarian.CONFIG['USE_EMAIL'] and not force:
            return False

        subject = event
        text = message
        oversize = False

        if files:
            if text[:15].lower() == '<!doctype html>':
                message = MIMEMultipart("related")
                message.attach(MIMEText(text, 'html'))
                if 'cid:logo' in text:
                    image_location = os.path.join(lazylibrarian.PROG_DIR, "data/images/ll.png")
                    with open(image_location, "rb") as fp:
                        img = MIMEImage(fp.read())
                    img.add_header("Content-ID", "<logo>")
                    message.attach(img)
            else:
                message = MIMEMultipart()
                message.attach(MIMEText(text, 'plain', "utf-8"))
        else:
            if text[:15].lower() == '<!doctype html>':
                message = MIMEText(text, 'html')
            else:
                message = MIMEText(text, 'plain', "utf-8")

        message['Subject'] = subject

        if is_valid_email(lazylibrarian.CONFIG['ADMIN_EMAIL']):
            from_addr = lazylibrarian.CONFIG['ADMIN_EMAIL']
        elif is_valid_email(lazylibrarian.CONFIG['EMAIL_FROM']):
            from_addr = lazylibrarian.CONFIG['EMAIL_FROM']
        else:
            logger.warn("Invalid FROM address, check config settings")
            return False

        if not to_addr:
            to_addr = lazylibrarian.CONFIG['EMAIL_TO']
            logger.debug("Using default to_addr=%s" % to_addr)
        if not is_valid_email(to_addr):
            logger.warn("Invalid TO address, check users email and/or config")
            return False

        message['From'] = formataddr(('LazyLibrarian', from_addr))
        message['To'] = to_addr
        message['Date'] = formatdate(localtime=True)
        message['Message-ID'] = "<%s@%s>" % (uuid.uuid4(), from_addr.split('@')[1])

        logger.debug('Email notification: %s' % message['Subject'])
        logger.debug('Email from: %s' % message['From'])
        logger.debug('Email to: %s' % message['To'])
        logger.debug('Email ID: %s' % message['Message-ID'])
        if text[:15].lower() == '<!doctype html>':
            logger.debug('Email text: %s' % text[:15])
        else:
            logger.debug('Email text: %s' % text)
        logger.debug('Files: %s' % files)

        if files:
            for f in files:
                fsize = check_int(os.path.getsize(syspath(f)), 0)
                limit = check_int(lazylibrarian.CONFIG['EMAIL_LIMIT'], 0)
                title = unaccented(os.path.basename(f))
                if limit and fsize > limit * 1024 * 1024:
                    oversize = True
                    msg = '%s is too large (%s) to email' % (title, fsize)
                    logger.debug(msg)
                    if lazylibrarian.CONFIG['CREATE_LINK']:
                        logger.debug("Creating link to %s" % f)
                        params = [lazylibrarian.CONFIG['CREATE_LINK'], f]
                        rc, res, err = run_script(params)
                        if res and res.startswith('http'):
                            msg = "%s is available to download, %s" % (title, res)
                            logger.debug(msg)
                    message.attach(MIMEText(msg))
                else:
                    subtype = mime_type(syspath(f)).split('/')[1]
                    logger.debug('Attaching %s %s' % (subtype, title))
                    with open(syspath(f), "rb") as fil:
                        part = MIMEApplication(fil.read(), _subtype=subtype, Name=title)
                        part['Content-Disposition'] = 'attachment; filename="%s"' % title
                        message.attach(part)

        try:
            # Create a secure SSL context
            context = ssl.create_default_context()
            # but allow no certificate check so self-signed work
            if not lazylibrarian.CONFIG['SSL_VERIFY']:
                context.check_hostname = False
                context.verify_mode = ssl.CERT_NONE

            if lazylibrarian.CONFIG['EMAIL_SSL']:
                mailserver = smtplib.SMTP_SSL(lazylibrarian.CONFIG['EMAIL_SMTP_SERVER'],
                                              check_int(lazylibrarian.CONFIG['EMAIL_SMTP_PORT'], 465),
                                              context=context)
            else:
                mailserver = smtplib.SMTP(lazylibrarian.CONFIG['EMAIL_SMTP_SERVER'],
                                          check_int(lazylibrarian.CONFIG['EMAIL_SMTP_PORT'], 25))

            if lazylibrarian.CONFIG['EMAIL_TLS']:
                mailserver.starttls(context=context)
            else:
                mailserver.ehlo()

            if lazylibrarian.CONFIG['EMAIL_SMTP_USER']:
                mailserver.login(lazylibrarian.CONFIG['EMAIL_SMTP_USER'],
                                 lazylibrarian.CONFIG['EMAIL_SMTP_PASSWORD'])

            logger.debug("Sending email to %s" % to_addr)
            mailserver.sendmail(from_addr, to_addr, message.as_string())
            mailserver.quit()
            if oversize:
                return False
            return True

        except Exception as e:
            logger.warn('Error sending Email: %s' % e)
            logger.error('Email traceback: %s' % traceback.format_exc())
            return False

        #
        # Public functions
        #

    def notify_message(self, subject, message, to_addr):
        return self._notify(message=message, event=subject, force=True, to_addr=to_addr)

    def email_file(self, subject, message, to_addr, files):
        logger.debug("to_addr=%s" % to_addr)
        res = self._notify(message=message, event=subject, force=True, files=files, to_addr=to_addr)
        return res

    def notify_snatch(self, title, fail=False):
        if lazylibrarian.CONFIG['EMAIL_NOTIFY_ONSNATCH']:
            if fail:
                return self._notify(message=title, event=notifyStrings[NOTIFY_FAIL])
            else:
                return self._notify(message=title, event=notifyStrings[NOTIFY_SNATCH])
        return False

    def notify_download(self, title, bookid=None, force=False):
        # suppress notifications if the notifier is disabled but the notify options are checked
        if not lazylibrarian.CONFIG['USE_EMAIL'] and not force:
            return False

        if lazylibrarian.CONFIG['EMAIL_NOTIFY_ONDOWNLOAD'] or force:
            files = None
            event = notifyStrings[NOTIFY_DOWNLOAD]
            logger.debug('Email send attachment is %s' % lazylibrarian.CONFIG['EMAIL_SENDFILE_ONDOWNLOAD'])
            if lazylibrarian.CONFIG['EMAIL_SENDFILE_ONDOWNLOAD']:
                if not bookid:
                    logger.debug('Email request to attach book, but no bookid')
                else:
                    filename = None
                    preftype = None
                    custom_typelist = get_list(lazylibrarian.CONFIG['EMAIL_SEND_TYPE'])
                    typelist = get_list(lazylibrarian.CONFIG['EBOOK_TYPE'])

                    if not lazylibrarian.CONFIG['USER_ACCOUNTS']:
                        if custom_typelist:
                            preftype = custom_typelist[0]
                            logger.debug('Preferred filetype = %s' % preftype)
                        elif typelist:
                            preftype = typelist[0]
                            logger.debug('Default preferred filetype = %s' % preftype)
                    else:
                        db = database.DBConnection()
                        cookie = cherrypy.request.cookie
                        if cookie and 'll_uid' in list(cookie.keys()):
                            res = db.match('SELECT BookType from users where UserID=?', (cookie['ll_uid'].value,))
                            if res and res['BookType']:
                                preftype = res['BookType']
                                logger.debug('User preferred filetype = %s' % preftype)
                            else:
                                logger.debug('No user preference for %s' % cookie['ll_uid'].value)
                        else:
                            logger.debug('No user login cookie')
                        if not preftype and typelist:
                            preftype = typelist[0]
                            logger.debug('Default preferred filetype = %s' % preftype)

                    db = database.DBConnection()
                    data = db.match('SELECT BookFile,BookName from books where BookID=?', (bookid,))
                    if data:
                        bookfile = data['BookFile']
                        types = []
                        if bookfile and path_isfile(bookfile):
                            basename, extn = os.path.splitext(bookfile)
                            for item in set(
                                    typelist + custom_typelist):
                                # Search download and email formats for existing book formats
                                target = basename + '.' + item
                                if path_isfile(target):
                                    types.append(item)

                            logger.debug('Available filetypes: %s' % str(types))

                            # if the format we want to send is available, select it
                            if preftype in types:
                                filename = basename + '.' + preftype
                                logger.debug('Found preferred filetype %s' % preftype)
                            # if the format is not available, see if it's a type we want to convert,
                            # otherwise send the first available format
                            else:
                                # if there is a type we want to convert from in the available formats,
                                # convert it
                                for convertable_format in get_list(lazylibrarian.CONFIG['EMAIL_CONVERT_FROM']):
                                    if convertable_format in types:
                                        logger.debug('Converting %s to preferred filetype %s' %
                                                     (convertable_format, preftype))
                                        # noinspection PyBroadException
                                        try:
                                            filename = ebook_convert.convert(basename + '.' + convertable_format,
                                                                             preftype)
                                            logger.debug('Converted %s to preferred filetype %s' %
                                                         (convertable_format, preftype))
                                            break
                                        except Exception:
                                            logger.debug("Conversion %s to %s failed" % (convertable_format, preftype))
                                            continue
                                # If no convertable formats found, revert to default behavior of sending
                                # first available format
                                else:
                                    logger.debug('Preferred filetype %s not found, sending %s' % (preftype, types[0]))
                                    filename = basename + '.' + types[0]

                        if force:
                            event = title
                            if filename:
                                title = lazylibrarian.NEWFILE_MSG.replace('{name}', data['BookName']).replace(
                                    '{method}', ' is attached').replace('{link}', '')
                            else:
                                title = lazylibrarian.NEWFILE_MSG.replace('{name}', data['BookName']).replace(
                                    '{method}', ' is not available').replace('{link}', '')
                        else:
                            title = data['BookName']
                        logger.debug('Found %s for bookid %s' % (filename, bookid))
                    else:
                        logger.debug('[%s] is not a valid bookid' % bookid)
                        data = db.match('SELECT IssueFile,Title,IssueDate from issues where IssueID=?', (bookid,))
                        if data:
                            filename = data['IssueFile']
                            title = "%s - %s" % (data['Title'], data['IssueDate'])
                            logger.debug('Found %s for issueid %s' % (filename, bookid))
                        else:
                            logger.debug('[%s] is not a valid issueid' % bookid)
                            filename = ''
                    if filename:
                        files = [filename]  # could add cover_image, opf
                        event = "LazyLibrarian Download"
            return self._notify(message=title, event=event, force=force, files=files)
        return False

    def test_notify(self, title='This is a test notification from LazyLibrarian'):
        if lazylibrarian.CONFIG['EMAIL_SENDFILE_ONDOWNLOAD']:
            db = database.DBConnection()
            data = db.match('SELECT bookid from books where bookfile <> ""')
            if data:
                return self.notify_download(title=title, bookid=data['bookid'], force=True)
        return self.notify_download(title=title, bookid=None, force=True)


notifier = EmailNotifier
